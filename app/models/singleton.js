define(['backbone'], function(Backbone) {
    console.log('Passing through singleton...')
    var Singleton = Backbone.Model.extend({});
    return new Singleton();
    
});